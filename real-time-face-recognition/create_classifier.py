import math
import os
import pickle

import numpy as np
import tensorflow as tf
from sklearn.svm import SVC

import sys
sys.path.insert(0, 'facenet')
import facenet

FACES_ALIGNED_PATH = 'real-time-face-recognition/data/faces/aligned/'
FACENET_MODELS_PATH = 'facenet/models/'
FACENET_MODEL_FILE_NAME = '20170512-110547.pb'
SVM_MODEL_PATH = 'real-time-face-recognition/models/'
SVM_MODEL_FILENAME = 'support_vector_model.pkl'
BATCH_SIZE = 100
IMAGE_SIZE = 160


def create_classifier():
    with tf.Session() as session:
        data_set = facenet.get_dataset(os.path.expanduser(FACES_ALIGNED_PATH))
        facenet.load_model(FACENET_MODELS_PATH + FACENET_MODEL_FILE_NAME)
        image_paths, labels = facenet.get_image_paths_and_labels(data_set)

        # Get embedding for each image..
        images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
        embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
        phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")

        number_of_images = len(image_paths)
        number_of_batches = int(math.ceil(number_of_images / BATCH_SIZE))
        embeddings_array = np.zeros((number_of_images, (embeddings.get_shape()[1])))
        for i in range(number_of_batches):
            start_index = i * BATCH_SIZE
            end_index = min((i + 1) * BATCH_SIZE, number_of_images)
            image_path_batch = image_paths[start_index:end_index]
            images_batch = facenet.load_data(image_path_batch, False, False, IMAGE_SIZE)
            embeddings_array[start_index:end_index, :] = session.run(embeddings,
                                                                     feed_dict={images_placeholder: images_batch,
                                                                                phase_train_placeholder: False})

        # Train classifier of embeddings..
        model = SVC(kernel='linear', probability=True)
        model.fit(embeddings_array, labels)

        # Create a list of class names
        class_names = [cls.name.replace('_', ' ') for cls in data_set]
        class_names.sort()

        # Save model
        with open(os.path.expanduser(SVM_MODEL_PATH + SVM_MODEL_FILENAME), 'wb') as outfile:
            pickle.dump((model, class_names), outfile)
