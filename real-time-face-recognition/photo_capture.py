import sys

import cv2
import os

from create_classifier import create_classifier
from align_images import align_faces

video_capture = cv2.VideoCapture(0)

try:
    class_name = sys.argv[1]
except IndexError:
    print("Usage: photo_capture.py <persons_name>")
    sys.exit(1)

photo_folder = 'real-time-face-recognition/data/faces/raw/' + class_name
directory = os.path.abspath(photo_folder)
if not os.path.exists(directory):
    os.makedirs(directory)

counter = 0


def print_info():
    cv2.putText(frame, "User: " + class_name,
                (20, 20), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 0, 0),
                thickness=1, lineType=2)
    cv2.putText(frame, "Photos taken: " + str(counter) + "/50",
                (20, 50), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 0, 0),
                thickness=1, lineType=2)
    cv2.putText(frame,  "(Press 'q' to take photo)",
                (20, 80), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 0, 0),
                thickness=1, lineType=2)


while counter < 50:
    ret, frame = video_capture.read()
    print_info()
    cv2.imshow('Video', frame)
    k = cv2.waitKey(1) & 0xFF
    if k == ord('q'):
        print(directory + '/image_' + str(counter) + '.jpg')
        cv2.imwrite(directory + '/image_' + str(counter) + '.jpg', frame)
        counter += 1

video_capture.release()
cv2.destroyAllWindows()
print("Aligning images..")
align_faces(class_name)
print("Images aligned.")
print("Creating classifier, please wait..")
create_classifier()
print("Classifier created.")
