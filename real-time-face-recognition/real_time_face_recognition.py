import numpy as np
import cv2
import pickle
import tensorflow as tf
import sys

sys.path.insert(0, 'facenet')
import facenet
import detect_face

VECTOR_MODEL = 'real-time-face-recognition/models/support_vector_model.pkl'
FACENET_MODELS_PATH = 'facenet/models/'
FACENET_MODEL_FILE_NAME = '20170512-110547.pb'
CONFIDENCE_THRESHOLD = 0.5
UNRECOGNISED_COLOUR = (0, 0, 255)
RECOGNISED_COLOUR = (0, 255, 0)
MIN_SIZE = 20
THRESHOLD = [0.6, 0.7, 0.7]
FACTOR = 0.709
MARGIN = 44
FRAME_INTERVAL = 3
INPUT_IMAGE_SIZE = 160


def box_out_of_frame(bounding_box, frame):
    return bounding_box[0] <= 0 or bounding_box[1] <= 0 or bounding_box[2] >= len(frame[0]) or bounding_box[3] >= len(
        frame)


def pre_process_image(bounding_box, frame, facenet):
    cropped_image = frame[bounding_box[1]:bounding_box[3], bounding_box[0]:bounding_box[2], :]
    flipped_image = facenet.flip(cropped_image, False)
    resize_image = cv2.resize(flipped_image, (INPUT_IMAGE_SIZE, INPUT_IMAGE_SIZE),
                              interpolation=cv2.INTER_CUBIC)
    whiten_image = facenet.prewhiten(resize_image)
    return whiten_image.reshape(-1, INPUT_IMAGE_SIZE, INPUT_IMAGE_SIZE, 3)


with tf.Session() as session:
    pnet, rnet, onet = detect_face.create_mtcnn(session, FACENET_MODELS_PATH)
    facenet.load_model(FACENET_MODELS_PATH + FACENET_MODEL_FILE_NAME)

    images_placeholder = tf.get_default_graph().get_tensor_by_name("input:0")
    embeddings = tf.get_default_graph().get_tensor_by_name("embeddings:0")
    phase_train_placeholder = tf.get_default_graph().get_tensor_by_name("phase_train:0")

    with open(VECTOR_MODEL, 'rb') as file:
        model, class_names = pickle.load(file)

    video_capture = cv2.VideoCapture(0)

    while True:
        _, frame = video_capture.read()
        frame = cv2.resize(frame, (0, 0), fx=0.75, fy=0.75)
        bounding_boxes, _ = detect_face.detect_face(frame, MIN_SIZE, pnet, rnet, onet, THRESHOLD, FACTOR)
        number_of_faces = bounding_boxes.shape[0]

        for i in range(number_of_faces):
            bounding_box = bounding_boxes[i][:].astype(int)

            if box_out_of_frame(bounding_box, frame):
                continue

            preprocessed_image = pre_process_image(bounding_box, frame, facenet)

            # Run current live frame through facenet to get embedding..
            feed_dict = {images_placeholder: preprocessed_image, phase_train_placeholder: False}
            embedding = np.zeros((1, (embeddings.get_shape()[1])))
            embedding[0, :] = session.run(embeddings, feed_dict=feed_dict)

            # Run embedding through SVM to get class probabilities..
            predictions = model.predict_proba(embedding)
            best_class_indices = np.argmax(predictions, axis=1)
            best_class_probabilities = predictions[np.arange(len(best_class_indices)), best_class_indices]

            if best_class_probabilities[0] > CONFIDENCE_THRESHOLD:
                colour = RECOGNISED_COLOUR
                identities_name = class_names[best_class_indices[0]]
            else:
                colour = UNRECOGNISED_COLOUR
                identities_name = "Unknown"

            cv2.rectangle(frame, (bounding_box[0], bounding_box[1]), (bounding_box[2], bounding_box[3]), colour, 2)
            cv2.putText(frame, identities_name, ((bounding_box[0]), (bounding_box[3] + 20)),
                        cv2.FONT_HERSHEY_COMPLEX_SMALL, 1, (0, 255, 0), thickness=1, lineType=2)

        cv2.imshow('Video', frame)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

video_capture.release()
cv2.destroyAllWindows()
