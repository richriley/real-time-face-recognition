import detect_face
import numpy as np
import os
import tensorflow as tf
from scipy import misc

import facenet

MIN_SIZE = 20
THRESHOLD = [0.6, 0.7, 0.7]
FACTOR = 0.709
IMAGE_SIZE = 182
DATA_PATH = "real-time-face-recognition/data/faces/"
FACENET_MODELS_PATH = 'facenet/models/'
RAW_DATA_PATH = DATA_PATH + "raw/"

def align_faces(class_name):
    output_path = DATA_PATH + "aligned/"
    output_directory = os.path.expanduser(output_path)
    if not os.path.exists(output_directory):
        os.makedirs(output_directory)

    raw_data = facenet.get_dataset(RAW_DATA_PATH)

    with tf.Graph().as_default():
        session = tf.Session(config=tf.ConfigProto(log_device_placement=False))
        with session.as_default():
            pnet, rnet, onet = detect_face.create_mtcnn(session, FACENET_MODELS_PATH)

    for classes in raw_data:
        output_class_dir = os.path.join(output_directory, classes.name)
        if not os.path.exists(output_class_dir):
            os.makedirs(output_class_dir)

        for image_path in classes.image_paths:
            filename = os.path.splitext(os.path.split(image_path)[1])[0]
            output_filename = os.path.join(output_class_dir, filename + '.png')

            if not os.path.exists(output_filename):
                try:
                    image = misc.imread(image_path)
                except (IOError, ValueError, IndexError) as e:
                    print(e)
                else:
                    image = image[:, :, 0:3]

                    bounding_boxes, _ = detect_face.detect_face(image, MIN_SIZE, pnet, rnet, onet, THRESHOLD, FACTOR)

                    detected_face = bounding_boxes[0, 0:4]
                    bounding_box = np.zeros(4, dtype=np.int32)
                    bounding_box[:] = detected_face[:]

                    cropped_face = image[bounding_box[1]:bounding_box[3], bounding_box[0]:bounding_box[2], :]
                    scaled_face = misc.imresize(cropped_face, (IMAGE_SIZE, IMAGE_SIZE), interp='bilinear')
                    misc.imsave(output_filename, scaled_face)
