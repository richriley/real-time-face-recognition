![Scheme](group_shot.png)
# Real Time Face Recognition

This a real time face recognition project that makes use of [David Sandberg's Implementation](https://github.com/davidsandberg/facenet) of the [FaceNet](https://arxiv.org/abs/1503.03832) classifier.

All the code found under the ./facenet folder is taken from David Sandberg's implementation.

All the code found under ./real-time-face-recognition is original.

To setup:  
`git clone https://richriley@bitbucket.org/richriley/real-time-face-recognition.git`  
`cd real-time-face-recognition`  
`conda env create -f real-time-face-recognition.yml`  
`conda activate real-time-face-recognition`  

To train the classifier on your face:  
`python real-time-face-recognition/photo_capture.py your-name`

To run the real time classifier:  
`python real-time-face-recognition/real_time_face_recognition.py`

The classifier will work on multiple faces at once, simply run the photo_capture.py again.

# How does it work?

FaceNet is a neural network that maps images of human faces to an 'embedding'. An embedding is simply a 128 dimension vector. FaceNet has been trained to map different photos of the same face to embeddings that are close togethor in vector space. 

During the training phase of this project (photo_capture.py) each photo captured is cropped to the users face, fed through FaceNet to produce it's embedding and then assigned a label (your name).

Once all the images are captured, we train a [support vector machine](https://en.wikipedia.org/wiki/Support_vector_machine) on the embeddings.

Put simply, the SVM creates a boundary between the different classes of embeddings, dividing the embeddings into groups (one for each person the system has been trained on).

Then, during run time (real_time_face_recognition.py) the system simply takes each live frame, feeds it through FaceNet to get the embedding, and then uses the SVM to see which group of embeddings it has the closest match to. This is the label that it then assigns to it.